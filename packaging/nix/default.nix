{ pkgs ? import <nixpkgs> { } }:

let
  git-gamble-derivation = pkgs.fetchurl {
    url = "https://gitlab.com/pinage404/git-gamble/-/raw/main/packaging/nix/git-gamble/default.nix";
    sha256 = "0000000000000000000000000000000000000000000000000000";
  };
  git-gamble = pkgs.callPackage git-gamble-derivation {
    version = "1.3.0";
    sha256 = "1111111111111111111111111111111111111111111111111111";
    cargoSha256 = "0000000000000000000000000000000000000000000000000000";
  };
in
pkgs.mkShell {
  buildInputs = [
    git-gamble
  ];
}

<!-- markdownlint-disable-next-line MD033 MD041 -->
<img src="https://gitlab.com/pinage404/git-gamble/-/raw/main/assets/logo/git-gamble.svg" width="100" title="git-gamble's logo" alt="git-gamble's logo" />

# Git-Gamble

<!-- markdownlint-disable-next-line MD039 MD045 -->
[![Crate available on Crates.io](https://img.shields.io/crates/v/git-gamble.svg?logo=rust) ![](https://img.shields.io/crates/d/git-gamble.svg?logo=rust)](https://crates.io/crates/git-gamble)
[![AppImage available on GitLab](https://img.shields.io/badge/dynamic/json?url=https%3A%2F%2Fcrates.io%2Fapi%2Fv1%2Fcrates%2Fgit-gamble%2Fversions&query=versions.0.num&label=AppImage&prefix=v&logo=linux)](https://gitlab.com/pinage404/git-gamble/-/packages)
[![Debian available on GitLab](https://img.shields.io/badge/dynamic/json?url=https%3A%2F%2Fcrates.io%2Fapi%2Fv1%2Fcrates%2Fgit-gamble%2Fversions&query=versions.0.num&label=Debian&prefix=v&logo=debian)](https://gitlab.com/pinage404/git-gamble/-/packages)

[![dependency status](https://deps.rs/crate/git-gamble/2.2.1/status.svg)](https://deps.rs/crate/git-gamble/2.2.1)
[![pipeline status](https://img.shields.io/gitlab/pipeline/pinage404/git-gamble?label=pipeline&logo=gitlab)](https://gitlab.com/pinage404/git-gamble/commits/main)
[![coverage report](https://gitlab.com/pinage404/git-gamble/badges/main/coverage.svg)](https://gitlab.com/pinage404/git-gamble/-/commits/main)
[![AppVeyor for Homebrew status](https://ci.appveyor.com/api/projects/status/qrd9p11ec2kbt1xs?svg=true)](https://ci.appveyor.com/project/pinage404/git-gamble)

[![License ISC](https://img.shields.io/crates/l/git-gamble.svg)](https://gitlab.com/pinage404/git-gamble/blob/main/LICENSE)
[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-v2.0%20adopted-ff69b4.svg)](code_of_conduct.md)

Blend [TCR (`test && commit || revert`)](https://rachelcarmena.github.io/2018/11/13/test-driven-programming-workflows.html) + [TDD (Test Driven Development)](https://www.wikiwand.com/en/Test-driven_development) to make sure to **develop** the **right** thing, **babystep by babystep**

[Original idea](https://github.com/FaustXVI/tcrdd) by Xavier Detant

<!-- markdownlint-capture -->
<!-- markdownlint-disable MD007 MD010 -->
- [Theory](#theory)
  - [TCR](#tcr)
  - [TDD](#tdd)
  - [TCRDD](#tcrdd)
- [How to install](#how-to-install)
  - [Requirements](#requirements)
  - [Installation methods](#installation-methods)
    - [AppImage](#appimage)
    - [Debian](#debian)
    - [Mac OS X / Homebrew](#mac-os-x-homebrew)
      - [Upgrade](#upgrade)
    - [Windows / Chocolatey](#windows-chocolatey)
    - [Nix / NixOS](#nix-nixos)
      - [Nix Flake](#nix-flake)
      - [Project level](#project-level)
        - [DirEnv](#direnv)
      - [Home-Manager / user level](#home-manager-user-level)
      - [NixOS / system level](#nixos-system-level)
    - [Cargo](#cargo)
    - [Download the binary](#download-the-binary)
  - [Check the installation](#check-the-installation)
- [How to use](#how-to-use)
  - [Usage](#usage)
  - [Shells completions](#shells-completions)
  - [Hooks](#hooks)
- [When to use it ?](#when-to-use-it-)
- [Backlog](#backlog)
  - [Distribution / publishing backlog](#distribution-publishing-backlog)
    - [Adding package to the X packages repository](#adding-package-to-the-x-packages-repository)
    - [CheckList](#checklist)
  - [Technical improvement opportunities](#technical-improvement-opportunities)
- [Reinvent the wheel](#reinvent-the-wheel)
- [Contributing](#contributing)
  - [Development](#development)
    - [Setup](#setup)
      - [Gitpod](#gitpod)
      - [Simple](#simple)
        - [Troubleshooting](#troubleshooting)
      - [Manual](#manual)
  - [Debug](#debug)
  - [Deployment](#deployment)
<!-- markdownlint-restore -->

## Theory

### TCR

_[**TCR** (`test && commit || revert`)](https://rachelcarmena.github.io/2018/11/13/test-driven-programming-workflows.html) is cool!_ It **encourages** doing **baby steps**, reducing the waste when we are wrong

But it **doesn't** allow us to **see** the **tests failing**

So:

- Maybe we test nothing (assert forgotten)

  ```python
  def test_should_be_Buzz_given_5():
      input = 5
      actual = fizz_buzz(input)
      # Oops! Assert has been forgotten
  ```

- Maybe we are testing something that is not the thing we should be testing

  ```typescript
  it("should be Fizz given 3", () => {
    const input = 3;
    const actual = fizzBuzz(input);
    expect(input).toBe("Fizz");
    // Oops! Asserts on the input value instead of the actual value
  });
  ```

### TDD

_[**TDD** (Test Driven Development)](https://www.wikiwand.com/en/Test-driven_development) is cool!_ It makes sure we **develop** the **right** thing, step by step

### TCRDD

**T**_CRDD_ = **T**_CR_ + **T**_DD_

TCRDD **blends** the constraints of the two **methods** to **benefit** from their **advantages**

Therefore, TCRDD makes sure we **develop** the **right** thing, step by step, and we are **encouraged** to do so by **baby steps**, reducing the waste when we are wrong

<!-- markdownlint-capture -->
<!-- markdownlint-disable MD010 -->
```plantuml
@startuml
skinparam ArrowColor black

start
repeat
	partition "red" #Coral {
		repeat
			:Write a test]
			:Gamble that the tests fail/
			if (Actually run tests) then (Fail)
				-[#Red]->
				:Commit;
				break
			else (Pass)
				-[#Green]->
				:Revert;
			endif
		repeat while (Write another test)
	}
	partition "green" #Lime {
		repeat
			:Write the minimum code]
			:Gamble that the tests pass/
			if (Actually run tests) then (Pass)
				-[#Green]->
				:Commit;
				break
			else (Fail)
				-[#Red]->
				:Revert;
			endif
		repeat while (Try something else)
	}
	partition "refactor" #668cff {
		repeat
			repeat
				:Write code
				without changing the behavior]
				:Gamble that the tests pass/
				if (Actually run tests) then (Pass)
					-[#Green]->
					:Commit;
					break
				else (Fail)
					-[#Tomato]->
					:Revert;
				endif
			repeat while (Change something else)
		repeat while (Another things to refactor ?)
	}
repeat while (Another feature to add ?)
stop
@enduml
```
<!-- markdownlint-restore -->

_If the above diagram is missing, you can see it [on GitLab](https://gitlab.com/pinage404/git-gamble#tcrdd)_

`git-gamble` is a tool that helps to use the TCRDD method

## How to install

### Requirements

`git-gamble` doesn't repackage `git`, it uses the one installed on your system

You have to [install `git` manually](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) if you install `git-gamble` using :

- AppImage
- Debian
- Cargo

Other installation methods include `git` as a dependency

Make sure it is available in your `$PATH`, you can check it with this command

```shell
git --help
```

### Installation methods

Installations are currently not really convenient, [contributions are welcome](#contributing), feel free to [add package to your prefered packages repository](#adding-package-to-the-x-packages-repository)

#### AppImage

1. Download the [latest version](https://gitlab.com/api/v4/projects/pinage404%2Fgit-gamble/packages/generic/git-gamble-AppImage/2.5.0/git-gamble-v2.5.0-x86_64.AppImage)

   ```shell
   curl --location "https://gitlab.com/api/v4/projects/pinage404%2Fgit-gamble/packages/generic/git-gamble-AppImage/2.5.0/git-gamble-v2.5.0-x86_64.AppImage" --output git-gamble-v2.5.0-x86_64.AppImage
   ```

1. Make it executable

   ```shell
   chmod +x git-gamble-v2.5.0-x86_64.AppImage
   ```

1. Put it your `$PATH`

   ```shell
   # for example
   mkdir -p ~/.local/bin
   ln git-gamble-v2.5.0-x86_64.AppImage ~/.local/bin/git-gamble
   export PATH+=":~/.local/bin"
   ```

#### Debian

1. Go to [the package registry page](https://gitlab.com/pinage404/git-gamble/-/packages)
1. Go to the latest version of `git-gamble-debian`
1. Download the latest version `git-gamble_2.5.0_amd64.deb`
1. Install package

   As **root**

   ```shell
   dpkg --install git-gamble*.deb
   ```

This is not really convenient but a better way will come when [GitLab Debian Package Manager MVC](https://gitlab.com/gitlab-org/gitlab/-/issues/5835) will be available

#### Mac OS X / Homebrew

[Install Homebrew](https://brew.sh/)

```shell
brew tap pinage404/git-gamble https://gitlab.com/pinage404/git-gamble.git
brew install --HEAD git-gamble
```

##### Upgrade

`git-gamble` has not yet been packaged by Homebrew

To upgrade `git-gamble`, run this command

```shell
brew reinstall git-gamble
```

#### Windows / Chocolatey

[Install Chocolatey](https://chocolatey.org/)

1. Go to [the package registry page](https://gitlab.com/pinage404/git-gamble/-/packages)
1. Go to the latest version of `git-gamble.portable`
1. Download the latest version `git-gamble.portable.2.5.0.nupkg`
1. Install package

   Follow [GitLab's documentation](https://docs.gitlab.com/ee/user/packages/nuget_repository/index.html) and [Chocolatey's documentation](https://docs.chocolatey.org/en-us/choco/commands/install)

   ```shell
   choco install ./git-gamble.portable.2.5.0.nupkg
   ```

#### Nix / NixOS

Installation can be done at different levels, see below

It can be done with [`nix flake`](#nix-flake) or with legacy way, in this case you will have to run command several times to change the `sha256` and `cargoSha256` by the given one by Nix

[Nix 2.4 or higher](https://github.com/yusdacra/nix-cargo-integration/issues/79#issuecomment-1140485371) is required

##### Nix Flake

Use without installing

```bash
nix run gitlab:pinage404/git-gamble -- --help
```

To install it, see [usage example](https://gitlab.com/pinage404/git-gamble/-/raw/main/packaging/nix/home-manager/flake.nix)

##### Project level

In `shell.nix`

```nix
{ pkgs ? import <nixpkgs> {} }:

let
  git-gamble-derivation = pkgs.fetchurl {
    url = "https://gitlab.com/pinage404/git-gamble/-/raw/main/packaging/nix/git-gamble/default.nix";
    sha256 = "0000000000000000000000000000000000000000000000000000";
  };
  git-gamble = pkgs.callPackage git-gamble-derivation {
    version = "2.5.0";
    sha256 = "1111111111111111111111111111111111111111111111111111";
    cargoSha256 = "0000000000000000000000000000000000000000000000000000";
  };
in
pkgs.mkShell {
  buildInputs = [
    git-gamble

    # others dependencies here
    pkgs.nodejs
  ];
}
```

Then run this command

```shell
nix-shell
```

###### DirEnv

To automate the setup of the environment it's recommanded to install [DirEnv](https://direnv.net/)

Add in `.envrc`

```direnv
use nix
```

Then run this command

```shell
direnv allow
```

##### Home-Manager / user level

Install [Home Manager](https://nix-community.github.io/home-manager/)

In `~/.config/nixpkgs/home.nix`

```nix
{ pkgs, ... }:

let
  git-gamble-derivation = pkgs.fetchurl {
    url = "https://gitlab.com/pinage404/git-gamble/-/raw/main/packaging/nix/git-gamble/default.nix";
    sha256 = "0000000000000000000000000000000000000000000000000000";
  };
  git-gamble = pkgs.callPackage git-gamble-derivation {
    version = "2.5.0";
    sha256 = "1111111111111111111111111111111111111111111111111111";
    cargoSha256 = "0000000000000000000000000000000000000000000000000000";
  };
in
{
  home.packages = [
    git-gamble

    # others dependencies here
    pkgs.gitAndTools.git-absorb
  ];
}
```

Then run this command

```shell
home-manager switch
```

##### NixOS / system level

In `/etc/nixos/configuration.nix`

```nix
{ pkgs, ... }:

let
  git-gamble-derivation = pkgs.fetchurl {
    url = "https://gitlab.com/pinage404/git-gamble/-/raw/main/packaging/nix/git-gamble/default.nix";
    sha256 = "0000000000000000000000000000000000000000000000000000";
  };
  git-gamble = pkgs.callPackage git-gamble-derivation {
    version = "2.5.0";
    sha256 = "1111111111111111111111111111111111111111111111111111";
    cargoSha256 = "0000000000000000000000000000000000000000000000000000";
  };
in
{
  environment.systemPackages = [
    git-gamble

    # others dependencies here
    pkgs.bat
  ];

  # This value determines the NixOS release with which your system is to be compatible, in order to avoid breaking some software such as database servers
  # You should change this only after NixOS release notes say you should
  system.stateVersion = "20.09";
}
```

Then run this command

```shell
nixos-rebuild switch
```

#### Cargo

[Install Cargo](https://doc.rust-lang.org/cargo/getting-started/installation.html)

```shell
cargo install git-gamble
```

Add `~/.cargo/bin` to your `$PATH`

Fish:

```fish
set --export --append PATH ~/.cargo/bin
```

Bash / ZSH:

```bash
export PATH=$PATH:~/.cargo/bin
```

#### Download the binary

Only for Linux and Windows `x86_64`

1. Download the binary on the [release page](https://gitlab.com/pinage404/git-gamble/-/releases/version%2F2.5.0)
2. Put it in your `$PATH`

### Check the installation

Check if all have been well settled

```shell
git gamble
```

If it has been **well settled**, it should output this :

```txt
error: The following required arguments were not provided:
    <--pass|--fail>
    <TEST_COMMAND>...

USAGE:
    git-gamble [OPTIONS] <--pass|--fail> [--] <TEST_COMMAND>...
    git-gamble [OPTIONS] <SUBCOMMAND>

For more information try --help
```

If it has been **badly settled**, it should output this :

```txt
git : 'gamble' is not a git command. See 'git --help'.
```

## How to use

To see all available flags and options

```shell
git-gamble --help # dash between `git` and `gamble` is only needed for --help
```

1. Write a failing test in your codebase, then :

   ```shell
   git gamble --fail -- $YOUR_TEST_COMMAND
   ```

1. Write the minimum code to make tests pass, then :

   ```shell
   git gamble --pass -- $YOUR_TEST_COMMAND
   ```

1. Refactor your code, then :

   ```shell
   git gamble --pass -- $YOUR_TEST_COMMAND
   ```

It's a bit tedious to always repeat the test command

So you can set an environment variable with the test command to avoid repeating it all the time

```shell
export GAMBLE_TEST_COMMAND="cargo test"
git gamble --pass
```

Test command must exit with a 0 status when there are 0 failing tests, anything else is considered as a failure

For more detailed example, this the [demo](demo/README.md)

[![asciicast](https://asciinema.org/a/496949.svg)](demo/README.md)

### Usage

`git-gamble --help`

<!-- BEGIN generated by git-gamble --help -->
```txt
git-gamble 2.5.0
Blend TCR (`test && commit || revert`) + TDD (Test Driven Development) to make sure to develop
the right thing, babystep by babystep

USAGE:
    git-gamble [OPTIONS] <--pass|--fail> [--] <TEST_COMMAND>...
    git-gamble [OPTIONS] <SUBCOMMAND>

ARGS:
    <TEST_COMMAND>...    The command to execute to know the result [env: GAMBLE_TEST_COMMAND=]

OPTIONS:
    -g, --pass
            Gamble that tests should pass [aliases: green, refactor]

    -r, --fail
            Gamble that tests should fail [aliases: red]

    -C, --repository-path <REPOSITORY_PATH>
            Repository path [default: .]

    -e, --edit
            Open editor to edit commit's message

        --fixup <FIXUP>
            Fixes up commit

    -h, --help
            Print help information

    -m, --message <MESSAGE>
            Commit's message [default: ]

    -n, --dry-run
            Do not make any changes

        --no-verify
            Do not run git hooks

        --squash <SQUASH>
            Construct a commit message for use with `rebase --autosquash`

    -V, --version
            Print version information

SUBCOMMANDS:
    generate-shell-completions    
    help                          Print this message or the help of the given subcommand(s)

Any contributions (feedback, bug report, merge request ...) are welcome
https://gitlab.com/pinage404/git-gamble
```
<!-- END generated by git-gamble --help -->

### Shells completions

To manually generate shell completions you can use this command

`git gamble generate-shell-completions --help`

<!-- BEGIN generated by git gamble generate-shell-completions --help -->
```txt
git-gamble-generate-shell-completions 

USAGE:
    git-gamble generate-shell-completions <SHELL>

ARGS:
    <SHELL>
            Put generated file here :
            * Fish https://fishshell.com/docs/current/completions.html#where-to-put-completions
            * Others shells ; Don't know, MR are welcome
            [possible values: bash, elvish, fish, powershell, zsh]

OPTIONS:
    -h, --help
            Print help information
```
<!-- END generated by git gamble generate-shell-completions --help -->

### Hooks

`git-gamble` provides his own custom [hooks](https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks) :

- `pre-gamble <GAMBLED>`
  - `pre-gamble` hook is executed with one argument `<GAMBLED>`
- `post-gamble <GAMBLED> <ACTUAL>`
  - `post-gamble` hook is executed with two arguments `<GAMBLED>` and `<ACTUAL>`

Where :

- `<GAMBLED>` is `pass` or `fail`
- `<ACTUAL>` is `pass` or `fail`

Custom hooks of `git-gamble` are like any other client-side [hooks](https://git-scm.com/docs/git-hook) :

- a hook is a file
- a hook must be in the `$GIT_DIR/hooks/` folder
  - or in the folder configured by `git config core.hooksPath`
- a hook must be executable (`chmod +x .git/hooks/*-gamble`)
- will not be executed if any of these options are used:
  - `--no-verify`
  - `--dry-run`

Check [the `hooks`' folder](https://gitlab.com/pinage404/git-gamble/-/blob/main/.config/git/hooks/) for examples of use

- [`post-gamble.real_time_collaboration.sample.sh`](https://gitlab.com/pinage404/git-gamble/-/blob/main/.config/git/hooks/post-gamble.real_time_collaboration.sample.sh) is specially adapted to _near_ **real-time collaboration**
- [`post-gamble.assistant.sample.sh`](https://gitlab.com/pinage404/git-gamble/-/blob/main/.config/git/hooks/post-gamble.assistant.sample.sh) is a simple assistant that displays tips based on the result of the gamble

The following diagram shows when custom hooks are executed in relation to normal git hooks

```mermaid
flowchart LR
    subgraph git-gamble's hooks' lifecyle
        direction TB
        git-gamble([git-gamble\n--pass OR --fail])
        --> pre-gamble[pre-gamble\npass OR fail]:::gitGambleHookStyle
        --> GAMBLE_TEST_COMMAND([exec $GAMBLE_TEST_COMMAND]):::gitGambleInternalStyle
        --> gamble{Result ?}:::gitGambleInternalStyle

        gamble -->|Success| Success
        subgraph Success
            direction TB

            git_add([git add --all]):::gitGambleInternalStyle
            --> git_commit([git commit]):::gitGambleInternalStyle
            --> pre-commit:::gitHookStyle
            --> prepare-commit-msg[prepare-commit-msg\n$GIT_DIR/COMMIT_EDITMSG\nmessage]:::gitHookStyle
            --> commit-msg[commit-msg\n$GIT_DIR/COMMIT_EDITMSG]:::gitHookStyle
            --> post-commit:::gitHookStyle
            post-commit --> rewritten?
            rewritten?{{"Last commit rewritten ?\nWhen gambling fail\nafter another gamble fail"}}:::gitGambleInternalStyle
            rewritten? -->|Yes| post-rewrite[post-rewrite\namend]:::gitHookStyle --> post-gamble-success
            rewritten? -->|No| post-gamble-success
            post-gamble-success[post-gamble\npass OR fail\nsuccess]:::gitGambleHookStyle
        end

        gamble -->|Error| Error
        subgraph Error
            direction TB

            git_reset([git reset --hard]):::gitGambleInternalStyle
            --> post-gamble-error[post-gamble\npass OR fail\nerror]:::gitGambleHookStyle
        end
    end

    subgraph Legend
        direction TB

        subgraph Legend_[" "]
            direction LR

            command([Command executed by user])
            git-gamble_command([Command executed by git-gamble]):::gitGambleInternalStyle
            condition{Condition ?}:::gitGambleInternalStyle
        end

        subgraph Hooks
            direction LR

            hook[git's hook]:::gitHookStyle
            hook_with_argument[git's hook\nfirst argument\nsecond argument]:::gitHookStyle
            git-gamble_hook_with_argument[git-gamble's hook\nfirst argument\nsecond argument]:::gitGambleHookStyle
        end
    end

    classDef gitHookStyle fill:#f05133,color:black,stroke:black;
    classDef gitGambleHookStyle fill:#5a3730,color:white,stroke:white;
    classDef gitGambleInternalStyle fill:#411d16,color:white,stroke:white;
```

## When to use it ?

- You want to :
  - learn nor practice TDD
  - learn nor practice TCR without [the drawbacks](#tcr)
  - practice a new language
- You follow the [Test F.I.R.S.T. Principles](https://dev.to/mundim/writing-your-f-i-r-s-t-unit-tests-1iop)

## Backlog

- [when revert -> `git clean` #3](https://gitlab.com/pinage404/git-gamble/-/issues/3)
- `git workspace` support
  - `git update-ref` should contain an unique identifier to the workspace
    - branche name ?
    - folder path ?
- gamble hooks
  - branch based developement
    - `git commit --fixup`
    - `git rebase --autosquash`
  - optional hook to revert if not gambled in a delay
- like git, flags & options & arguments should be retrieved from CLI or environment variable or config's file
  - re-use `git config` to store in file ?
  - repository level config using direnv and environment variable ?
- [stash instead of revert ?](https://rachelcarmena.github.io/2018/11/13/test-driven-programming-workflows.html#my-proposal-of-tcr-variant)
- shell completion
  - in the package ?
    - [ ] Cargo
    - [ ] AppImage
    - [ ] Chocolatey
  - for `git gamble` not only `git-gamble`

### Distribution / publishing backlog

- OS distribution
  - Linux
    - RPM
      - [`cargo-rpm`](https://crates.io/crates/cargo-rpm)
      - `nix bundle`
    - make AppImage updatable
      - bintray-zsync|pinage404|git-gamble-AppImages|git-gamble|git-gamble-\_latestVersion-x86_64.AppImage.zsync
    - SnapCraft ?
      - [snapcraft's docs about rust](https://snapcraft.io/docs/rust-applications)
    - FlatPak ?
    - [`fpm` : tool that help to generate to several packages](https://github.com/jordansissel/fpm)
    - document AppImage with firejail ?
    - document AppImage with bubblewrap ?
    - [Open Build Service](https://build.opensuse.org/) seems to be a service that build for every Linux targets
      - it can be [use](https://en.opensuse.org/openSUSE:Build_Service_Clients)
        - thougth REST
          - badly documented
        - with `ocs` a CLI who seems to be a VCS like SVN (commit = commit + push)
          - who use the REST API under the hood
  - Container Image (Docker) ?
  - [Awesome Rust Deployment](https://github.com/rust-unofficial/awesome-rust#deployment)
  - [Awesome Rust Platform Specific](https://github.com/rust-unofficial/awesome-rust#platform-specific)
- permanent URL to download latest version
  - symlinks to URL containing the version name ?
  - using GitLab Pages ?
- versioned Homebrew Formula
  - Use [Cargo-Release](https://github.com/sunng87/cargo-release/blob/master/docs/faq.md#how-do-i-update-my-readme-or-other-files) to bump version
  - how to update sha256 in the versioned file ?

#### Adding package to the X packages repository

Where the X packages repository is e.g. Nixpgks, Debian, Homebrew, Chocolatey ...

Feel free to do it, we don't plan to do it at the moment, it's too long to learn and understand how each of them works

If you do it, please [file an issue](https://gitlab.com/pinage404/git-gamble/-/issues/new) or [open an MR](https://gitlab.com/pinage404/git-gamble/-/merge_requests/new) to update the documentation

#### CheckList

- [ ] package in local
- [ ] package in CI
- [ ] upload
- [ ] make public available
- [ ] complete how to install
- [ ] manual test
- [ ] update backlog

### Technical improvement opportunities

- licence check
  - cargo license
  - cargo deny
- smaller binary
  - cargo bloat
  - cargo deps
- refactor to [iterate over `Shell` `enum`'s values instead of having an hard-coded array](https://gitlab.com/pinage404/git-gamble/-/blob/6bd96ce884afe677ad50671e95631609962ca74a/src/build.rs#L26)

## Reinvent the wheel

> Why reinvent the wheel?
>
> This [script](https://github.com/FaustXVI/tcrdd) already works well

Because i would like to learn Rust `¯\_(ツ)_/¯`

## Contributing

[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-v2.0%20adopted-ff69b4.svg)](code_of_conduct.md)

Any contributions ([feedback, bug report](https://gitlab.com/pinage404/git-gamble/-/issues), [merge request](https://gitlab.com/pinage404/git-gamble/-/merge_requests) ...) are welcome

Respect the [code of conduct](code_of_conduct.md)

Follow [Keep a Changelog](https://keepachangelog.com/en/1.1.0/)

### Development

#### Setup

To contribute with merge request

##### Gitpod

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/pinage404/git-gamble)

##### Simple

Install [Direnv](https://direnv.net/)

Install [Nix](https://nixos.org/guides/install-nix.html)

Let `direnv` automagically set up the environment by executing the following command in the project directory

```shell
direnv allow
```

###### Troubleshooting

If you get an error like this one when entering in the folder

```text
direnv: loading ~/Project/git-gamble/.envrc
direnv: using nix
direnv: using cached derivation
direnv: eval /home/pinage404/Project/git-gamble/.direnv/cache-.336020.2128d0aa28e
direnv: loading ./script/setup_variables.sh
   Compiling git-gamble v2.4.0-alpha.0 (/home/pinage404/Project/git-gamble)
direnv: ([/nix/store/19arfqh2anf3cxzy8zsiqp08xv6iq6nl-direnv-2.29.0/bin/direnv export fish]) is taking a while to execute. Use CTRL-C to give up.
error: linker `cc` not found
  |
  = note: No such file or directory (os error 2)

error: could not compile `git-gamble` due to previous error
```

Just run the following command to fix it

```sh
direnv reload
```

Help wanted to permanently fix this

##### Manual

- [Rustup](https://rustup.rs/)
- [clippy](https://crates.io/crates/clippy)
- [rustfmt](https://github.com/rust-lang/rustfmt)
- [rustfix](https://github.com/rust-lang/rustfix)
- [PlantUML](https://plantuml.com/fr/) if you want to see diagrams

### Debug

There are some logs in the programs, [`pretty_env_logger`](https://docs.rs/pretty_env_logger) is used to display them

There are 5 levels of logging (from the lightest to the most verbose) :

- `error`
- `warn`
- `info`
- `debug`
- `trace`

The `git-gamble` logs are "hidden" behind the `with_log` [feature](https://doc.rust-lang.org/cargo/reference/features.html)

The option `--features with_log` (or `--all-features`) must be added to each `cargo` command for which you want to see logs (e.g.) :

```sh
cargo build --all-features
cargo run --all-features
cargo test --all-features
```

Then, to display logs, add this environment variable :

```sh
export RUST_LOG="git_gamble=debug"
```

To display _really_ **everything** :

```sh
export RUST_LOG="trace"
```

There are other possibilities for logging in the [`env_logger` documentation](https://docs.rs/env_logger/)

### Deployment

Just run [cargo release](https://crates.io/crates/cargo-release) (in a wide terminal, in order to have `--help` not truncated in [the usage section](#usage))

```sh
cargo release
```

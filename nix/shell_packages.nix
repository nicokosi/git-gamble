{ pkgs }:

let
  nixWithFlakes = (
    pkgs.writeShellScriptBin "nix" ''
      exec ${pkgs.nix}/bin/nix --experimental-features "nix-command flakes" "$@"
    ''
  );
in

with pkgs;
[
  # rust environment
  gcc

  # needed by `cargo build --release`
  glibc

  # needed by git-gamble's tests
  bash
  shunit2

  # needed by git-gamble itself
  git

  # needed only to know the test coverage
  cargo-tarpaulin

  # needed only when releasing a new version
  cargo-release

  # needed only to check crates vulnerabilities
  cargo-audit

  # needed only to detect what can be improved
  cargo-bloat
  cargo-diet

  # needed to generate documentation schema
  plantuml

  # needed by VSCode extension to format Nix files
  rnix-lsp

  # bash still usable in interactive mode
  bashInteractive

  # replace `nix` with `nix` with flake enabled
  nixWithFlakes
]

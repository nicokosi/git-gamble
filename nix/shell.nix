# with import ./debug.nix { };

# `nix develop`

{ pkgs
, default ? { }
}:

{
  packages = (default.packages or [ ]) ++ import ./shell_packages.nix {
    pkgs = pkgs;
  };

  commands = (default.commands or [ ]) ++ [
    {
      category = "Git Gamble";
      name = "fail";
      command = "git-gamble --fail";
      help = "Gamble that tests should fail";
    }
    {
      category = "Git Gamble";
      name = "pass";
      command = "git-gamble --pass";
      help = "Gamble that tests should pass";
    }

    {
      category = "Git Gamble TDD";
      name = "red";
      command = "git-gamble --red";
      help = "Gamble that tests should fail";
    }
    {
      category = "Git Gamble TDD";
      name = "green";
      command = "git-gamble --green";
      help = "Gamble that tests should pass";
    }
    {
      category = "Git Gamble TDD";
      name = "refactor";
      command = "git-gamble --refactor";
      help = "Gamble that tests should pass";
    }
  ];

  devshell.startup.setup_variables.text = ''
    source ./script/setup_variables.sh
  '';
}

use std::io;
use tempfile::tempdir;
use test_log::test;

pub mod fixture;
use fixture::TestRepository;

#[test]
fn runs_hooks_when_not_passing_no_verify() -> io::Result<()> {
	let temporary_directory = tempdir()?;
	let test_repository = TestRepository::init_dirty(temporary_directory.path().to_path_buf())?;
	test_repository.add_hook("pre-commit", "false")?;

	let commit_message = "some message";
	let command =
		test_repository.execute_cli_with(&["--pass", "--message", commit_message, "--", "true"]);

	command.failure();
	assert!(test_repository.is_dirty());
	assert!(test_repository.has_no_new_commit());
	Ok(())
}

#[test]
fn does_not_run_hooks_when_passing_no_verify() -> io::Result<()> {
	let temporary_directory = tempdir()?;
	let test_repository = TestRepository::init_dirty(temporary_directory.path().to_path_buf())?;
	test_repository.add_hook("pre-commit", "false")?;

	let commit_message = "some message";
	let command = test_repository.execute_cli_with(&[
		"--pass",
		"--message",
		commit_message,
		"--no-verify",
		"--",
		"true",
	]);

	command.success();
	assert!(test_repository.is_clean());
	assert!(test_repository.has_new_commit());
	Ok(())
}

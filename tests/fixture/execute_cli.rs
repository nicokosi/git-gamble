use assert_cmd::assert::Assert;
use assert_cmd::Command;
use std::str;

pub fn execute_cli() -> Command {
	Command::cargo_bin(env!("CARGO_PKG_NAME")).unwrap()
}

pub fn execute_cli_with(args: &[&str]) -> Assert {
	log::info!("{} {}", env!("CARGO_PKG_NAME"), args.join(" "));
	execute_cli().args(args).assert()
}

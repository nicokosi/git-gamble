use std::env;
use std::io;
use tempfile::tempdir;

pub mod fixture;
use fixture::execute_cli_with;
use fixture::TestRepository;

#[test_log::test]
fn should_use_current_working_directory_by_default() -> io::Result<()> {
	let temporary_directory = tempdir()?;
	let repository = TestRepository::init_dirty(temporary_directory.path().to_path_buf())?;

	assert!(env::set_current_dir(repository.path()).is_ok());
	execute_cli_with(&["--pass", "--", "true"]).success();

	assert!(repository.is_clean());
	assert!(repository.changes_remain_in_the_working_file());
	assert!(repository.has_new_commit());
	Ok(())
}

mod repository_path_can_be_elsewhere {
	use super::*;
	use test_case::test_case;

	#[test_case("--repository-path")]
	#[test_case("-C")]
	#[test_log::test]
	fn should_use_path_when_giving_it_in_option(option: &str) -> io::Result<()> {
		let temporary_directory = tempdir()?;
		let repository = TestRepository::init_dirty(temporary_directory.path().to_path_buf())?;

		let path = repository.path().to_str().unwrap();
		execute_cli_with(&[option, path, "--pass", "--", "true"]).success();

		assert!(repository.is_clean());
		assert!(repository.changes_remain_in_the_working_file());
		assert!(repository.has_new_commit());
		Ok(())
	}
}

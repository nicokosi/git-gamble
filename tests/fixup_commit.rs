use speculoos::assert_that;
use std::io;
use tempfile::tempdir;

pub mod fixture;
use fixture::TestRepository;

#[test_log::test]
fn fixup_commit() -> io::Result<()> {
	let temporary_directory = tempdir()?;
	let repository = TestRepository::init_dirty(temporary_directory.path().to_path_buf())?;

	let command = repository.execute_cli_with(&["--pass", "--fixup", "@", "--", "true"]);

	command.success();
	let commit_message = "fixup! first commit";
	assert_that(&repository.head_message()).is_equal_to(commit_message.to_owned() + "\n");
	Ok(())
}

#[test_log::test]
fn fixup_commit_with_commit_search() -> io::Result<()> {
	let temporary_directory = tempdir()?;
	let repository = TestRepository::init_dirty(temporary_directory.path().to_path_buf())?;

	let command = repository.execute_cli_with(&["--pass", "--fixup", ":/initial", "--", "true"]);

	command.success();
	let commit_message = "fixup! initial commit";
	assert_that(&repository.head_message()).is_equal_to(commit_message.to_owned() + "\n");
	Ok(())
}

use speculoos::assert_that;
use std::io;
use tempfile::tempdir;

pub mod fixture;
use fixture::TestRepository;

mod edit_commit_message {
	use super::*;
	use test_case::test_case;

	#[test_case("-e")]
	#[test_case("--edit")]
	#[test_log::test]
	fn open_editor_when_committing(flag: &str) -> io::Result<()> {
		let temporary_directory = tempdir()?;
		let repository = TestRepository::init_dirty(temporary_directory.path().to_path_buf())?;

		let commit_message = "message";
		let command =
			repository.execute_cli_with_editor(&["--pass", flag, "--", "true"], commit_message);

		command.success();
		assert_that(&repository.head_message()).is_equal_to(commit_message.to_owned() + "\n");
		Ok(())
	}
}

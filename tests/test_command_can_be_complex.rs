use std::env;
use std::io;
use std::path::PathBuf;
use tempfile::tempdir;
use test_case::test_case;

pub mod fixture;
use fixture::TestRepository;

#[test_log::test]
fn command_can_have_arguments() -> io::Result<()> {
	let temporary_directory = tempdir()?;
	let repository = TestRepository::init_dirty(temporary_directory.path().to_path_buf())?;

	repository
		.execute_cli_with(&[
			"--pass",
			"--",
			"sh",
			"-c",
			"test 'hello world' = 'hello world'",
		])
		.success();

	assert!(repository.is_clean());
	assert!(repository.changes_remain_in_the_working_file());
	assert!(repository.has_new_commit());
	Ok(())
}

#[test_case("--pass", "tests/fixture/test_pass.sh")]
#[test_case("--fail", "tests/fixture/test_fail.sh")]
#[test_log::test]
fn command_can_be_a_script(gambling_flag: &str, script: &str) -> io::Result<()> {
	let temporary_directory = tempdir()?;
	let repository = TestRepository::init_dirty(temporary_directory.path().to_path_buf())?;

	let script_path = PathBuf::from(env!("CARGO_MANIFEST_DIR")).join(script);
	let script = script_path.to_str().unwrap();

	repository
		.execute_cli_with(&[gambling_flag, "--", "sh", script])
		.success();

	assert!(repository.is_clean());
	assert!(repository.changes_remain_in_the_working_file());
	assert!(repository.has_new_commit());
	Ok(())
}

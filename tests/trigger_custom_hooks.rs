use speculoos::assert_that;
use speculoos::prelude::ResultAssertions;
use speculoos::prelude::StrAssertions;
use std::fs::read_to_string;
use std::io;
use tempfile::tempdir;

pub mod fixture;
use fixture::TestRepository;

mod trigger_custom_hooks {
	use super::*;

	mod pre_gamble_hook_pass_gambling_option_to_hook {
		use super::*;

		#[test_log::test]
		fn when_gambling_pass() -> io::Result<()> {
			let temporary_directory = tempdir()?;
			let test_repository =
				TestRepository::init_dirty(temporary_directory.path().to_path_buf())?;

			let hook_output_directory = tempdir()?;
			let hook_output_path = hook_output_directory.path().join("hook_output");
			test_repository.add_hook(
				"pre-gamble",
				format!("echo \"$0 $*\" >>{}", hook_output_path.display()).as_str(),
			)?;

			let command = test_repository.execute_cli_with(&["--pass", "--", "true"]);

			command.success();
			let hook_output = read_to_string(hook_output_path);
			assert_that(&hook_output)
				.is_ok()
				.contains(".git/hooks/pre-gamble pass");
			Ok(())
		}

		#[test_log::test]
		fn when_gambling_fail() -> io::Result<()> {
			let temporary_directory = tempdir()?;
			let test_repository =
				TestRepository::init_dirty(temporary_directory.path().to_path_buf())?;

			let hook_output_directory = tempdir()?;
			let hook_output_path = hook_output_directory.path().join("hook_output");
			test_repository.add_hook(
				"pre-gamble",
				format!("echo \"$0 $*\" >>{}", hook_output_path.display()).as_str(),
			)?;

			let command = test_repository.execute_cli_with(&["--fail", "--", "true"]);

			command.success();
			let hook_output = read_to_string(hook_output_path);
			assert_that(&hook_output)
				.is_ok()
				.contains(".git/hooks/pre-gamble fail");
			Ok(())
		}
	}

	mod post_gamble_hook_pass_gambling_option_and_test_result_to_hook {
		use super::*;

		#[test_log::test]
		fn when_gambling_fail_and_test_fail() -> io::Result<()> {
			let temporary_directory = tempdir()?;
			let test_repository =
				TestRepository::init_dirty(temporary_directory.path().to_path_buf())?;

			let hook_output_directory = tempdir()?;
			let hook_output_path = hook_output_directory.path().join("hook_output");
			test_repository.add_hook(
				"post-gamble",
				format!("echo \"$0 $*\" >>{}", hook_output_path.display()).as_str(),
			)?;

			let command = test_repository.execute_cli_with(&["--fail", "--", "false"]);

			command.success();
			let hook_output = read_to_string(hook_output_path);
			assert_that(&hook_output)
				.is_ok()
				.contains(".git/hooks/post-gamble fail fail");
			Ok(())
		}

		#[test_log::test]
		fn when_gambling_pass_and_test_pass() -> io::Result<()> {
			let temporary_directory = tempdir()?;
			let test_repository =
				TestRepository::init_dirty(temporary_directory.path().to_path_buf())?;

			let hook_output_directory = tempdir()?;
			let hook_output_path = hook_output_directory.path().join("hook_output");
			test_repository.add_hook(
				"post-gamble",
				format!("echo \"$0 $*\" >>{}", hook_output_path.display()).as_str(),
			)?;

			let command = test_repository.execute_cli_with(&["--pass", "--", "true"]);

			command.success();
			let hook_output = read_to_string(hook_output_path);
			assert_that(&hook_output)
				.is_ok()
				.contains(".git/hooks/post-gamble pass pass");
			Ok(())
		}

		#[test_log::test]
		fn when_gambling_fail_but_test_pass() -> io::Result<()> {
			let temporary_directory = tempdir()?;
			let test_repository =
				TestRepository::init_dirty(temporary_directory.path().to_path_buf())?;

			let hook_output_directory = tempdir()?;
			let hook_output_path = hook_output_directory.path().join("hook_output");
			test_repository.add_hook(
				"post-gamble",
				format!("echo \"$0 $*\" >>{}", hook_output_path.display()).as_str(),
			)?;

			let command = test_repository.execute_cli_with(&["--fail", "--", "true"]);

			command.success();
			let hook_output = read_to_string(hook_output_path);
			assert_that(&hook_output)
				.is_ok()
				.contains(".git/hooks/post-gamble fail pass");
			Ok(())
		}

		#[test_log::test]
		fn when_gambling_pass_but_test_fail() -> io::Result<()> {
			let temporary_directory = tempdir()?;
			let test_repository =
				TestRepository::init_dirty(temporary_directory.path().to_path_buf())?;

			let hook_output_directory = tempdir()?;
			let hook_output_path = hook_output_directory.path().join("hook_output");
			test_repository.add_hook(
				"post-gamble",
				format!("echo \"$0 $*\" >>{}", hook_output_path.display()).as_str(),
			)?;

			let command = test_repository.execute_cli_with(&["--pass", "--", "false"]);

			command.success();
			let hook_output = read_to_string(hook_output_path);
			assert_that(&hook_output)
				.is_ok()
				.contains(".git/hooks/post-gamble pass fail");
			Ok(())
		}
	}
}

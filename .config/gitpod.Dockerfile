FROM gitpod/workspace-nix

USER root

RUN install-packages direnv

USER gitpod

RUN echo 'eval "$(direnv hook bash)"' \
    >>~/.bashrc

RUN echo 'eval "$(direnv hook zsh)"' \
    >>~/.zshrc

RUN mkdir --parents ~/.config/fish/ && \
    echo 'direnv hook fish | source' \
    >>~/.config/fish/config.fish

#!/usr/bin/env sh

GAMBLED="$1"
ACTUAL="$2"

if [ "pass" = "${GAMBLED}" ] && [ "pass" = "${ACTUAL}" ]; then
    echo "You can push"
elif [ "fail" = "${GAMBLED}" ] && [ "fail" = "${ACTUAL}" ]; then
    echo "Do not push"
elif [ "fail" = "${GAMBLED}" ] && [ "pass" = "${ACTUAL}" ]; then
    echo "The new test should verify a new behavior"
elif [ "pass" = "${GAMBLED}" ] && [ "fail" = "${ACTUAL}" ]; then
    echo "Maybe try a smaller step, if your test is too hard you can delete it (git reset --hard @~) and then refactor to help yourself with your next step https://twitter.com/KentBeck/status/250733358307500032"
fi

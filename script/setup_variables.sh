#!/usr/bin/env bash

# default `git-gamble`'s command check, format and test the code
export GAMBLE_TEST_COMMAND="sh -c 'cargo clippy --all-targets --all-features ; cargo check --all-targets --all-features ; ./tests/test_scripts.sh && cargo test --all-features'"

# allow to use `git-gamble` to build `git-gamble`
export PATH+=":./target/debug"
export PATH+=":./target/release"

# debug
# export RUST_LOG="trace"
# export RUST_TRACE="full"
# export RUST_BACKTRACE="1"
# export RUST_BACKTRACE="full"

use clap::ArgGroup;
use clap::Parser;
use clap_complete::Shell;
use std::path::PathBuf;

#[derive(Parser, Debug)]
pub struct GenerateShellCompletions {
	#[clap(
		arg_enum,
		long_help = "Put generated file here :\n* Fish https://fishshell.com/docs/current/completions.html#where-to-put-completions\n* Others shells ; Don't know, MR are welcome\n"
	)]
	pub shell: Shell,
}

#[derive(Parser, Debug)]
pub enum OptionalSubcommands {
	GenerateShellCompletions(GenerateShellCompletions),
}

#[derive(Parser, Debug)]
#[clap(
	version,
	infer_subcommands = true,
	subcommand_negates_reqs = true,
	group = ArgGroup::new("gambling").required(true),
	about = "Blend TCR (`test && commit || revert`) + TDD (Test Driven Development) to make sure to develop\nthe right thing, babystep by babystep",
	after_help = "Any contributions (feedback, bug report, merge request ...) are welcome\nhttps://gitlab.com/pinage404/git-gamble",
)]
pub struct App {
	// subcommands
	#[clap(subcommand)]
	pub optional_subcommands: Option<OptionalSubcommands>,

	// flags
	/// Gamble that tests should pass
	#[clap(
		short = 'g',
		long,
		group = "gambling",
		visible_aliases = &["green", "refactor"],
		display_order = 1,
	)]
	pub pass: bool,
	/// Gamble that tests should fail
	#[clap(
		short = 'r',
		long,
		group = "gambling",
		visible_alias = "red",
		display_order = 1
	)]
	pub fail: bool,
	/// Do not make any changes
	#[clap(short = 'n', long)]
	pub dry_run: bool,
	/// Do not run git hooks
	#[clap(long)]
	pub no_verify: bool,

	// options
	/// Repository path
	// reflect `git -C <repository-path>`
	#[clap(short = 'C', long, default_value = ".")]
	pub repository_path: PathBuf,

	/// Commit's message
	#[clap(short = 'm', long, default_value = "")]
	pub message: String,

	/// Open editor to edit commit's message
	#[clap(short = 'e', long)]
	pub edit: bool,

	/// Fixes up commit
	#[clap(long)]
	pub fixup: Option<String>,

	/// Construct a commit message for use with `rebase --autosquash`
	#[clap(long)]
	pub squash: Option<String>,

	// rest arguments
	/// The command to execute to know the result
	#[clap(
		env = "GAMBLE_TEST_COMMAND",
		multiple_occurrences = true,
		last = true,
		required = true
	)]
	pub test_command: Vec<String>,
}

#[test]
fn verify_app() {
	use clap::IntoApp;

	App::command().debug_assert()
}

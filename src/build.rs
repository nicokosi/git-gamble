use clap_complete::generate_to;
use clap_complete::Shell;
use std::env;
use std::fs;
use std::io;

mod app;

mod generate_shell_completions;
use generate_shell_completions::generate_shell_completions;

fn main() -> io::Result<()> {
	pretty_env_logger::init();

	let out_dir = env::var("SHELL_COMPLETIONS_DIR")
		.or_else(|_| env::var("OUT_DIR"))
		.expect(
			"Neither \"SHELL_COMPLETIONS_DIR\" or \"OUT_DIR\" environment variables were provided",
		);
	let out_dir = out_dir.as_str();

	log::info!("generate shell completions in {}", &out_dir);
	fs::create_dir_all(out_dir)?;

	let shells = [
		Shell::Bash,
		Shell::Elvish,
		Shell::Fish,
		Shell::PowerShell,
		Shell::Zsh,
	];
	for shell in shells {
		generate_shell_completions(|package_name, command| {
			generate_to(shell, command, package_name, out_dir)
				.expect("Can't generate shell completions");
		})
	}

	Ok(())
}
